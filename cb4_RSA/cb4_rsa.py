#version 1.0 build #1
# coding: utf-8

#https://gist.github.com/nickromano/3967431
#https://cryptii.com/base64-to-hex


# ## Creating KEY using RSA 


import base64
from Crypto.PublicKey import RSA
from Crypto import Random
from random import randint


# # Library

def create_my_keys(Public_file_path,Private_file_path,client_id):
    """
    This function will create 2 keys based on RSA encryption methodology
    public key to encrypt
    private key to decrypt
    
    input:
    client_id = string with no space
    """
    client_id=client_id.replace(" ", "")
    # generate new key
    random_generator = Random.new().read
    key = RSA.generate(2048, random_generator)
    public_key = key.publickey()
    
    # create id for the keys
    x_id = str(randint(1, 10000))

    #export public key
    f = open(Public_file_path+'pubkey_'+client_id+'_'+x_id+'.pem','w')
    f.write(key.publickey().exportKey('PEM').decode("utf-8"))
    f.close()
    # export PRIVATE key to file
    f = open(Private_file_path+'privatekey_'+client_id+'_'+x_id+'.pem','w')
    f.write(key.exportKey('PEM').decode("utf-8"))
    f.close()
    return 'two files generated'
def encrypt_file(input_file_path,Public_file_path,encrypted_file_path=""):
    """
    This function encrypt a file based on the public key sent from a client   
    """
    #load public key
    f = open(Public_file_path,'r')
    load_public = RSA.importKey(f.read().encode('utf-8'))
    f.close()
    load_public
    
    #load a file to be enc
    in_filename = input_file_path
    f = open(in_filename, 'rb')
    text = f.read()
    f.close()
    text
    
    #encrypted_file_path if not specified
    if encrypted_file_path=="":
        encrypted_file_path=input_file_path+'.enc'
    else :
        encrypted_file_path=encrypted_file_path+'.enc'
    
    #Export secret file
    out_filename = encrypted_file_path
    f = open(out_filename, 'wb')
    enc_data=load_public.encrypt(text, 32)
    f.write(base64.b64encode(enc_data[0]))
    f.close()
    yoursequence=enc_data[0]
    #type(enc_data[0])
    #enc_data[0].decode('utf-8')
    enc_data[0]
    return 'response'
def decrypt_file(encrypted_file_path,private_key_path,decrypted_file_path=""):
    """
    This function decrypt a file based on the private the client posses and was encrypted by matching public
    sent to the encryptor server
    """
    #load private key
    f = open(private_key_path,'r')
    loaded_private_key = RSA.importKey(f.read().encode('utf-8'))
    f.close()
    loaded_private_key
    
    #Import secret file
    enc_file= encrypted_file_path
    f = open(enc_file, 'rb')
    enc_data64 = f.read()
    f.close()
    enc_data64

    #decrypted_file_path if not specified
    if decrypted_file_path=="":
        decrypted_file_path=encrypted_file_path+'.dec'
    else :
        decrypted_file_path=decrypted_file_path+'.dec'
    
    #Decode and Export file
    dec_file= decrypted_file_path
    enc_data=base64.b64decode(enc_data64)
    enc_data
    #enc_data
    dec_data=loaded_private_key.decrypt(enc_data).decode('utf-8')

    f = open(dec_file, 'w')
    f.write(dec_data)
    f.close()
    return 'resposne'
