#Version: General 1.0 Built #1

#libraries
import pandas as pd

def my_login(file_path,position=1):
    """
    this function with load a password file in file_path 
    and return user and password in the line of the position mentioned default is 1
    """
    table=pd.read_csv(file_path,sep='\t')
    [user,password] = [ table.loc[position-1,'user'],table.loc[position-1,'password']]
    return [user,password]
