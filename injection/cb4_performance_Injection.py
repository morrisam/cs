#Version: Injection 1.0 Built #1 
# coding: utf-8

#libraries
import pandas as pd
import numpy as np
from sqlalchemy import create_engine
import pymssql
from datetime import datetime
#import psycopg2

# # ROI Injection Staging 

# ## Function list 


def logger_a(log_file_path,*args):
    """
    Credit to Ben Nichols
    This function records the intermeidate results of the code processes
    To insert a line break into the log.txt file, simply run the fucntion without any args
    """
    
    line='\n'

    #evaluate if the function was called with or without arguments. If yes then generate the text, else line break
    if len(args) > 0:        
        tms = datetime.now().isoformat(sep=' ')+' '
        args_flat = ' | '.join([str(arg) for arg in args])
        line = tms+'| '+args_flat
    

    #open the log.txt file in memory find the first line insert a line break and log the current text
    #file = "D:\Processes\Response_Reports\log.txt"
    with open(log_file_path, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)

    #end logger function



def connection_sql(server_type,server,server_username,server_password,db):
    """
    This function get parameters pointing to sql table and loading table from SQL to pandas DF
    type can be "mssql" or "postgres"
    connection string will support either
    """
    connection_string=''
    try:
        if server_type=='mssql':
            connection_string = 'mssql+pymssql://'+server_username+':'+server_password+'@'+server+'/'+db
        elif server_type=='postgresql':        
            connection_string = 'postgresql://'+server_username+':'+server_password+'@'+server+'/'+db
        return  create_engine(connection_string);
    except Exception as e:
        raise Exception("Connection to {} failed! ".format(server),e)
        return "Connection to {} failed! ".format(server);
    #postgresql

def qry_imp(sql_engine,table_name):
    #test connection
    #qry = """select * from {table}""".format(table_name)
    qry = "SELECT * FROM %s" % (table_name)
    return pd.read_sql(qry, sql_engine);

def print_imp(imp,path=""):
    """
    Print implementation report based on agreement with rnd team for injection
    """
    #print CSV
    #dictionary = {'\\n':'','\\r':'' ,'\r':'','\n':'','|':'_'}
    dictionary = {'\\n':'','\\r':'','\n':'','\r':'','\|':'_'}
    imp.replace(dictionary, regex=True, inplace=True)
    profile_name=imp['cretail_profile_name'].unique()[0]
    date_pull=datetime.today().strftime('%Y%m%d')
    filename='imp_{pn}_{d}.csv'.format(pn=profile_name,d=date_pull)
    imp.to_csv(path+filename, index=False,sep='|')
    return filename;

def create_mapping_file(mapping_template_path,footer_df,output_mapping_instance_path):
    """
    This function will load a file that is saved the location of the
    mapping_template_path and will add to end of the file rows from
    footer_df. basically a concatenation. then save the file in the
    output_mapping_instance_path in a pipe delimiter exclusive of the
    header in the CSV
    """
    f_mapping_template= open(mapping_template_path,'r')
    #print (f_mapping_template.read())
    #output file 
    f_output= open(output_mapping_instance_path,"w+")
    f_output.write(f_mapping_template.read())
    footer_df.to_csv(f_output,sep='|',header=False, index=False,float_format='%.2f')
    f_output.close()
    
    return;
def return_row_configuration(config_df, rownum):
    """
        This should return a dictionary of the requested row from a df.
        Args:
            path: Path to input csv file location
            rownumber: A row number in the df 0..n 
        Returns:
            Dict: The function return output a dictionary of the df row
        Raises:
            IndexError: If you put an index out of range (ex: rownumber) you will receive the following error:
              'list index out of range'.
              
    """
    return config_df.loc[rownum,:].to_dict()


def per_report_summary(df):
 
    """this function return a summary of the df (implementation report).
    the format will be text and numbers.
    definition
 
    nrow = number of rows in the data frame,
    minweek = min of the ImplementationPeriodStart column ,
    maxweek = max of ImplementationPeriodEnd column
    totalRev = sum of (revenueincrease) where row
          has value 1 in summable (check if 1 is a string or number)
    store_count = number of unique stores in the StoreIdentifier column
    iterations_count = number of unique iterationID
 
    expected output:
 
    s = file with *a* rows, with performance report with evaluated
        periods between *b* to *c*, that include *f* iterations, with
        *e* stores and total revenue of *d*.
 
    """
     
    nrow = len(df.index)
    minweek = df['ImplementationPeriodStart'].min()
    maxweek = df['ImplementationPeriodEnd'].max()
 
    totalRev=pd.to_numeric(df.loc[df['Summable']==1,'RevenueIncrease']).sum()
    #xxdf = pd.to_numeric(df['RevenueIncrease'])
    #d = xxdf[xxdf > 0.0].sum()
     
    store_count = df['StoreIdentifier'].nunique()
    iterations_count = df['IterationId'].nunique()
 
 
    s = "file with {:,} rows, contain performance report that have evaluated periods "        "between {} and {}, that include {:,} iterations, with "        "{:,} stores and total revenue of {:,.2f}".format(nrow,minweek,maxweek,iterations_count,store_count,totalRev)
     
    return s;



# In[ ]:


def get_customers(active=True, config='config.csv'):
    """
    This function returns the customers present in the configuration file.
    Active flag is by default set to true, but can be set to false
    """
    #grab configuration
    config_df = pd.read_csv(config, delimiter='\t', dtype=str)
    
    if active==True:
        config_df = config_df[config_df['is_live'] == '1']

    #logger('Printed Customer List','Active Flag: '+str(active))
    
    return [i for i in config_df['Customer']]


def get_host_from_url(s):
    #s='https://us-prod3.c-b4.com/c-application-circlek'
    try:
        s=s.replace('https://','')
        pos=s.find('/')
    except:
        s='';
        pos=0
    return s[0:pos]
#get_host_from_url(s)
#config_df['cRetail_url'].apply(get_host_from_url)

def create_per_config_df(per_list,config_df):
    """
    this function takes 
    1) response report config file and 
    2) Per report configuration file 
    and binds them together
    """
    per_list['profile_name_lower'] = per_list['profile_name'].str.lower()
    config_df['Customer_lower'] = config_df['Customer'].str.lower()
    per_config=pd.merge(left=per_list,right=config_df,how='inner',left_on='profile_name_lower',right_on='Customer_lower')
    per_config['host_name']=per_config['cRetail_url'].apply(get_host_from_url)
    columns=['approved','is_live','type','profile_name_lower','profile_name','server_type','server_name','roi_db','cretail_profile_name','Customer','Project_Name','cRetail_username','cRetail_password','host_name','cRetail_url']
    return per_config[columns]


