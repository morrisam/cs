﻿#!/usr/bin/env python 
# -*- coding: utf-8 -*-
#

import datetime
import json
import os
import sys
import time
import urllib2

##### injection parameters #####
sftpUser = "delivery"
sftpPassword = "assaf"
sftpHost = "us-ftp.c-b4.com"
injectionRootPath = "/public_ftp/roi_injection/cb4_mart/"
mappingFile = "cb4_mapping.csv"
dataFile = "cb4_report.csv"
errorFile = "callback/error/error.csv"
successFile = "callback/success/success.csv"
dsId = "5"
profile = "cbmart"
retailUser = "analyst"
retailPassword = "password"
csvDelimiter = "|"
#################################

payload = {
    # used if no specific override path provided
    "basePath": "sftp://" + sftpUser + ":" + sftpPassword + "@" + sftpHost + ":22/" + injectionRootPath,

    # example of relative file path - mandatory
    "mappingFile": mappingFile,

    # example of relative path with sub folder - mandatory
    "dataFile": dataFile,

    # can be null or missing, in which case no erorr file would written
    "errorFile": errorFile,

    # example of specific override
    # can be null or missing, in which case no success file would written
    "successFile": successFile,

    # mandatory and should match dsId in path URL
    "dsId": dsId,
    # must match customer profile name so for example in 'c-application-wesco' the profile name is 'wesco' (evaluate using endsWith on context)
    "profile": profile
}
reqData = json.dumps(payload, sort_keys=True, indent=4)

auth = retailUser + ":" + retailPassword
headersMap = {
    "Accept": "*/*",
    "User-Agent": "CB4/ROIInjection",
    "Authorization": "Basic " + auth.encode("base64").rstrip(),
    "Content-Type": "application/json"
}

req = urllib2.Request("http://localhost/c-application-" + profile + "/rest/dashboard/upload/" + dsId + "/data/preprocess?delimiter=" + csvDelimiter, headers=headersMap, data=reqData)
try:
    rsp = urllib2.urlopen(req)
    rspContent = rsp.read()
    reply = json.loads(rspContent)
    print(json.dumps(reply, indent=2, sort_keys=True))
except urllib2.HTTPError, err:
    print(err.read())
    print(err.reason)
    sys.exit(1)
except urllib2.URLError, err:
    print("Some other error happend:", err.reason)
    sys.exit(1)
